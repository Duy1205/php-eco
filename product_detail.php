<?php
    include_once('header.php')
?>

<nav id="sticky" class="header-bottom theme1 d-none d-lg-block">
    <div class="container">
        <div class="row align-items-center">
            <div class="  position-relative">
                <ul class="main-menu d-flex">
                <?php 
                    require_once("entities/product.class.php");
                    require_once("entities/category.class.php");

                    if( !isset($_GET["cateid"]) ){
                        $prods = Product::list_product();
                    }else{
                        $cateid = $_GET["cateid"];
                        $prods = Product::list_product_by_cateid( $cateid );
                    }

                    $cates = Category::list_category();
                    
                    foreach ( $cates as $item){
                        echo "<li > <a href=/LAB03/list_product.php?cateid=".$item["CateID"]."> ".$item["CategoryName"]." </a> </li>";
                    }
                ?>

                </ul>
            </div>
        </div>
    </div>
</nav>
    
<?php
    require_once("entities/product.class.php");
    require_once("entities/category.class.php");

    if( !isset($_GET["id"]) ){
        header('Location: not_found.php');
    }else{
        $id = $_GET["id"];
        $prod = Product::get_product($id);
        $prods = reset($prod);
        $prods_relate = Product::list_product_relate( $prods["CateID"], $id);
    }
    $cates = Category::list_category();
?>

    <div class="container">
        <div class="row" >
        <h3>Chi Tiết Sản Phẩm</h3>
            <div class = "col-7">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row g-0">

                        <div class="col-md-8">
                            <img src="/LAB03/<?php echo $prods["Picture"]; ?>" width="350px" height="350px" alt="banner-thumb-naile">
                        </div>
                        
                        <div class="col-md-4">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $prods["ProductName"];?></h5>
                                <p class="card-text"><?php echo $prods["Price"];?> VND</p>
                                <p class="card-text">Description: <?php echo $prods["Description"];?></p>
                                <button type="button" class="btn btn-warning" onclick="location.href='/LAB03/shopping_cart.php?id=<?php echo $item["ProductID"]; ?>'">Mua Hang</button>
                            </div>
                        </div>

                    </div>
                </div>   
            </div>
        </div>

        <div class="row">

        <h3>Sản Phẩm Liên Quan</h3>

            <div class="row row-cols-1 row-cols-md-2 g-4">

            <?php
                foreach($prods_relate as $item){
            ?>

                <div class = "col-7">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-8">
                                <img src="/LAB03/<?php echo $item["Picture"]; ?>" width="350px" height="350px" alt="banner-thumb-naile">
                            </div>
                            <div class="col-md-4">
                                <div class="card-body">
                                    <h5 class="card-title"><a href="/LAB03/product_detail.php?cateid=<?php echo $item["CateID"];?>&id=<?php echo $item["ProductID"];?>"><?php echo $item["ProductName"];?></a></h5>
                                    <p class="card-text"><?php echo $item["Price"];?> VND</p>
                                    <p class="card-text">Description: <?php echo $item["Description"];?></p>
                                    <button type="button" class="btn btn-warning" onclick="location.href='/LAB03/shopping_cart.php?id=<?php echo $item["ProductID"]; ?>'">Mua Hàng</button>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    <?php
        }
    ?>

<?php
    include_once('footer.php')
?>