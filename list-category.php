<?php
    include_once('nav-bar.php');
    include_once('side-bar.php');
    include_once('config/db.class.php');
?>
<div class="main-panel">
    <div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                    <h4 class="card-title">Danh Sách Danh Mục</h4>
                    <table class="table table-success">
                        <thead>
                        <tr>
                            <th> STT </th>
                            <th> Name </th>
                            <th> </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                                    require_once("entities/product.class.php");
                                    require_once("entities/category.class.php");

                                    if( !isset($_GET["cateid"]) ){
                                        $prods = Product::list_product();
                                    }else{
                                        $cateid = $_GET["cateid"];
                                        $prods = Product::list_product_by_cateid( $cateid );
                                    }

                                    $cates = Category::list_category();
                                    
                                    foreach ( $cates as $item){
                                        echo "
                                        <tr class='table-success'>
                                        <td>".$item['CateID']." </td>
                                        <td>".$item['CategoryName']." </td>
                                        <td> 
                                            <button  class='btn btn-icons btn-rounded btn-outline-info btnEdit'>
                                                <i class='mdi mdi-pencil'></i>
                                            </button>
                                            <button  class='btn btn-icons btn-rounded btn-outline-warning btnDelete'>
                                                <i class='mdi mdi-close'></i>
                                            </button>
                                        </td>
                                    </tr>
                                        ";
                                    }
                                ?>
                        <!-- <tr class="table-warning">
                            <td> 2 </td>
                            <td> Messsy Adam </td>
                            <td> Flash </td>
                            <td> $245.30 </td>
                            <td> July 1, 2015 </td>
                        </tr>
                        <tr class="table-danger">
                            <td> 3 </td>
                            <td> John Richards </td>
                            <td> Premeire </td>
                            <td> $138.00 </td>
                            <td> Apr 12, 2015 </td>
                        </tr>
                        <tr class="table-success">
                            <td> 4 </td>
                            <td> Peter Meggik </td>
                            <td> After effects </td>
                            <td> $ 77.99 </td>
                            <td> May 15, 2015 </td>
                        </tr>
                        <tr class="table-primary">
                            <td> 5 </td>
                            <td> Edward </td>
                            <td> Illustrator </td>
                            <td> $ 160.25 </td>
                            <td> May 03, 2015 </td>
                        </tr> -->
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    // include_once('footer.php');
?>
<script>
    $(document).on("click", ".btnEdit", function(){
        let value =  $(this).attr("value");
        let id =  $(this).attr("_idCate");
        
        console.log({ id });
        window.location.href = `/ecommerce-php/views/dashboards/page/list-category.php?quanly=${value}&id=${id}`
    })
    $(document).on("click", ".btnDelete", function(){
        let value =  $(this).attr("value");
        let id =  $(this).attr("_idCate");
        
        console.log({ id });
        window.location.href = `/ecommerce-php/views/dashboards/page/list-category.php?${value}&id=${id}`
    })
</script>