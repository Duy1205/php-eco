<?php
    include_once('header.php')
?>
    <div class="container">
        <div class="row" >
        <div class = "col-7">

                <h1> Danh Sách Sản Phẩm </h1>

                <nav id="sticky" class="header-bottom theme1 d-none d-lg-block">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="  position-relative">
                                <ul class="main-menu d-flex">
                                <?php 
                                    require_once("entities/product.class.php");
                                    require_once("entities/category.class.php");

                                    if( !isset($_GET["cateid"]) ){
                                        $prods = Product::list_product();
                                    }else{
                                        $cateid = $_GET["cateid"];
                                        $prods = Product::list_product_by_cateid( $cateid );
                                    }

                                    $cates = Category::list_category();
                                    
                                    foreach ( $cates as $item){
                                        echo "<li > <a href=/LAB03/list_product.php?cateid=".$item["CateID"]."> ".$item["CategoryName"]." </a> </li>";
                                    }
                                ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>

            </div>
                <table class="table">
                    <thead>
                        <tr>
                            <!-- <th scope="col">ProductID</th> -->
                            <th scope="col">Tên Sản Phẩm</th>
                            <th scope="col">Loại</th>
                            <th scope="col">Giá</th>
                            <th scope="col">Số Lượng</th>
                            <th scope="col">Mô Tả Sản Phẩm</th>
                            <th scope="col">Hình Ảnh</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        require_once("entities/product.class.php");
                        require_once("entities/category.class.php");

                        if( !isset($_GET["cateid"]) ){
                            $prods = Product::list_product();
                        }else{
                            $cateid = $_GET["cateid"];
                            $prods = Product::list_product_by_cateid( $cateid );
                        }

                        $cates = Category::list_category();
                        
                        foreach($prods as $item){
                    ?>
                            <tr>
                                <td><a href="/LAB03/product_detail.php?cateid=<?php echo $item["CateID"];?>&id=<?php echo $item["ProductID"];?>"><?php echo $item["ProductName"];?></a> </td>
                                <th><?php echo $item["CateID"];?></th>
                                <td><?php echo $item["Price"];?></td>
                                <td><?php echo $item["Quantity"];?></td>
                                <td><?php echo $item["Description"];?></td>
                                <td>
                                    <a href="/LAB03/product_detail.php?cateid=<?php echo $item["CateID"];?>&id=<?php echo $item["ProductID"];?>"> <img src="/LAB03/<?php echo $item["Picture"];?>" width="100px" height="100px" alt=""></a>
                                </td>
                                <td> 
                                    <button  class="btn btn-warning" type="button" onclick="location.href='/LAB03/shopping_cart.php?id=<?php echo $item["ProductID"]; ?>'">Mua Hàng</button>
                                </td>
                            </tr>
                    <?php
                        }
                    ?>


                    </tbody>
                </table> 
            </div>
            
        </div>
        
    </div>
<?php
    include_once('footer.php')
?>
