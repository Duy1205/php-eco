<?php
    include_once('nav-bar.php');
    include_once('side-bar.php');
    include_once('config/db.class.php');
?>

<div class="main-panel">
    <div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">

		<div class="col-lg-12 stretch-card">
			<div class="card">
				<div class="card-body">
				<h4 class="card-title">Danh Sách Sản Phẩm</h4>
				<table class="table table-success">
					<thead>
					<tr>
						<th>Tên Sản Phẩm</th>
						<th>Loại</th>
						<th>Giá</th>
						<th>Số Lượng</th>
						<th>Mô Tả Sản Phẩm</th>
						<th>Hình Ảnh</th>
					</tr>
					</thead>
					<tbody>
					<?php
								require_once("entities/product.class.php");
								require_once("entities/category.class.php");

								if( !isset($_GET["cateid"]) ){
									$prods = Product::list_product();
								}else{
									$cateid = $_GET["cateid"];
									$prods = Product::list_product_by_cateid( $cateid );
								}

								$cates = Category::list_category();
								
								foreach($prods as $item){
							?>
						<tr class="table-success">
						<td><a href="/LAB03/product_detail.php?cateid=<?php echo $item["CateID"];?>&id=<?php echo $item["ProductID"];?>"><?php echo $item["ProductName"];?></a> </td>
										<th><?php echo $item["CateID"];?></th>
										<td><?php echo $item["Price"];?></td>
										<td><?php echo $item["Quantity"];?></td>
										<td><?php echo $item["Description"];?></td>
										<td>
											<a href="/LAB03/product_detail.php?cateid=<?php echo $item["CateID"];?>&id=<?php echo $item["ProductID"];?>"> <img src="/LAB03/<?php echo $item["Picture"];?>" width="100px" height="100px" alt=""></a>
										</td>
							<td> 
								<button type="button" _idPro="<?php echo $row_sanpham['sanpham_id'] ?>" value="capnhat" class="btn btn-icons btn-rounded btn-outline-info btnEdit">
									<i class="mdi mdi-pencil"></i>
								</button>
								<button type="button" _idPro="<?php echo $row_sanpham['sanpham_id'] ?>" value="xoa" class="btn btn-icons btn-rounded btn-outline-warning btnDelete">
									<i class="mdi mdi-close"></i>
								</button>
							</td>
						</tr>
					<?php
						} 
					?>
					<!-- <tr class="table-warning">
						<td> 2 </td>
						<td> Messsy Adam </td>
						<td> Flash </td>
						<td> $245.30 </td>
						<td> July 1, 2015 </td>
					</tr>
					<tr class="table-danger">
						<td> 3 </td>
						<td> John Richards </td>
						<td> Premeire </td>
						<td> $138.00 </td>
						<td> Apr 12, 2015 </td>
					</tr>
					<tr class="table-success">
						<td> 4 </td>
						<td> Peter Meggik </td>
						<td> After effects </td>
						<td> $ 77.99 </td>
						<td> May 15, 2015 </td>
					</tr>
					<tr class="table-primary">
						<td> 5 </td>
						<td> Edward </td>
						<td> Illustrator </td>
						<td> $ 160.25 </td>
						<td> May 03, 2015 </td>
					</tr> -->
					</tbody>
				</table>
				</div>
			</div>
		</div>
		</div>
		</div>
		</div>


<?php
    // include_once('../partials/footer.php');
?>
<script>
    $(document).on("click", ".btnEdit", function(){
        let value =  $(this).attr("value");
        let id =  $(this).attr("_idPro");
        
        window.location.href = `/ecommerce-php/views/dashboards/page/list-product.php?quanly=${value}&id=${id}`
    })
    $(document).on("click", ".btnDelete", function(){
        let value =  $(this).attr("value");
        let id =  $(this).attr("_idPro");
        
        window.location.href = `/ecommerce-php/views/dashboards/page/list-product.php?${value}&id=${id}`
    })
</script>