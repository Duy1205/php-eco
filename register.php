<?php
    include_once('./config/db_backup.class.php');
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    @import url(https://fonts.googleapis.com/css?family=Roboto:300);

    .login-page {
    width: 360px;
    padding: 8% 0 0;
    margin: auto;
    }
    .form {
    position: relative;
    z-index: 1;
    background: #FFFFFF;
    max-width: 360px;
    margin: 0 auto 100px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
    }
    .form input {
    font-family: "Roboto", sans-serif;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 14px;
    }
    .form button {
    font-family: "Roboto", sans-serif;
    text-transform: uppercase;
    outline: 0;
    background: #4CAF50;
    width: 100%;
    border: 0;
    padding: 15px;
    color: #FFFFFF;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
    }
    .form button:hover,.form button:active,.form button:focus {
    background: #43A047;
    }
    .form .message {
    margin: 15px 0 0;
    color: #b3b3b3;
    font-size: 12px;
    }
    .form .message a {
    color: #4CAF50;
    text-decoration: none;
    }
    .form .register-form {
    display: none;
    }
    .container {
    position: relative;
    z-index: 1;
    max-width: 300px;
    margin: 0 auto;
    }
    .container:before, .container:after {
    content: "";
    display: block;
    clear: both;
    }
    .container .info {
    margin: 50px auto;
    text-align: center;
    }
    .container .info h1 {
    margin: 0 0 15px;
    padding: 0;
    font-size: 36px;
    font-weight: 300;
    color: #1a1a1a;
    }
    .container .info span {
    color: #4d4d4d;
    font-size: 12px;
    }
    .container .info span a {
    color: #000000;
    text-decoration: none;
    }
    .container .info span .fa {
    color: #EF3B3A;
    }
    body {
    background: #76b852; /* fallback for old browsers */
    background: -webkit-linear-gradient(right, #76b852, #8DC26F);
    background: -moz-linear-gradient(right, #76b852, #8DC26F);
    background: -o-linear-gradient(right, #76b852, #8DC26F);
    background: linear-gradient(to left, #76b852, #8DC26F);
    font-family: "Roboto", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;      
    }
</style>
<body>
<div class="login-page">
  <div class="form">
    <form method="POST" enctype="multipart/form-data" class="login-form">
        <input name="txtName" type="text" placeholder="Name"/>
      <input name="txtEmail" type="text" placeholder="Email"/>
      <input name="txtPassword" type="password" placeholder="Password"/>
    <input name="txtPassword2" type="password" placeholder="Confirm Password"/>
      <button type="submit" name="btnRegister">Đăng ký</button>
      <p class="message">Not registered? <a href="#">Create an account</a></p>
    </form>
    <?php
        if(isset($_POST['btnRegister'])){
            $name        = $_POST['txtName'];
            $email        = $_POST['txtEmail'];
            $password     = $_POST['txtPassword'];
            $password2    = $_POST['txtPassword2'];
            if( $password != $password2){
            ?>  
                <script>
                    alert("2 password không đúng");
                </script>
            <?php
            }else{
            $passworda   = md5($password);
            $sql_insert_user = mysqli_query($con,"INSERT INTO users(UserName, Email, Password) values ('$name','$email','$passworda')");
            // $result   = mysqli_fetch_array($sql_user);
            if(!$sql_insert_user ){
                ?>
                    <script>
                        alert("Mời bạn đăng ký lại","Thông báo");
                    </script>
                <?php
            }
            else{
                ?>
                    <script>
                    alert("bạn đã đăng ký thành công","Thông báo");
                    setTimeout(() => {
                        window.location.href = "/lab03/login.php"
                    }, 700);
                    </script>
                <?php
            }
            }
        }
        ?>
  </div>
</div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script>
    
</script>