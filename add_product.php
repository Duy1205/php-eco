
<?php 
    include_once('nav-bar.php');
    include_once('side-bar.php');
    include_once('config/db.class.php');
    


    require_once("entities/product.class.php");
    require_once("entities/category.class.php");

    if(isset($_POST["btnSubmit"])){
        // echo "aaaaa";
        $productName = $_POST["txtName"];
        $cateID      = $_POST["txtCateID"];
        $price       = $_POST["txtPrice"];
        $quantity    = $_POST["txtQuantity"];
        $description = $_POST["txtDesc"];
        $picture     = $_FILES["txtPicture"];
        // echo gettype( $picture );
        // foreach ( $picture as $item ){
        //     echo  $item->name ;
        // }
        // echo  $picture ;
        $newProduct = new Product($productName, $cateID, $price, $quantity, $description, $picture);
        
        $result = $newProduct->save();
    }
?>

<?php
    if(isset($_GET["inserted"])){
        echo "<h2> Thêm Sản Phẩm Thành Công </h2>";
    }
?>
<div class="main-panel">
    <div class="content-wrapper">
    <div class="row">
    <div class="col-lg-12 stretch-card">
        <div class="card">
            <div class="card-body">
            <h4 class="card-title">Thêm Sản Phẩm</h4>
            
            <div class="container">
                <form method="POST" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tên Sản Phẩm</label>
                        <input type="text" name="txtName" value="<?php echo isset($_POST["txtName"]) ? $_POST["txtName"] : "" ;?>" class="form-control" id="lblinput" aria-describedby="emailHelp">
                        <div id="emailHelp"  class="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Mô Tả Sản Phẩm</label>
                        <input type="text" name="txtDesc" value="<?php echo isset($_POST["txtDesc"]) ? $_POST["txtDesc"] : "" ;?>" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Danh Mục</label>
                        <select class="form-select" aria-label="Default select example" name="txtCateID">
                            <?php 
                                $cates = Category::list_category();
                                foreach ( $cates as $item){
                                    echo "<option value=".$item["CateID"].">".$item["CategoryName"]."</option>";
                                }
                            ?>
                        </select>
                        <!-- <input type="text" name="txtCateID" value="<?php echo isset($_POST["txtCateID"]) ? $_POST["txtCateID"] : "" ;?>" class="form-control" id="exampleInputPassword1"> -->
                    </div><div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Giá</label>
                        <input type="text" name="txtPrice" value="<?php echo isset($_POST["txtPrice"]) ? $_POST["txtPrice"] : "" ;?>" class="form-control" id="exampleInputPassword1">
                    </div><div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Số Lượng</label>
                        <input type="text" name="txtQuantity" value="<?php echo isset($_POST["txtQuantity"]) ? $_POST["txtQuantity"] : "" ;?>" class="form-control" id="exampleInputPassword1">
                    </div><div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Hình Ảnh</label>
                        <input type="file" accept=".PNG,.GIF,.JPG" id="txtPicture" name="txtPicture" value="<?php echo isset($_POST["txtPicture"]) ? $_POST["txtPicture"] : "" ;?>" class="form-control" id="exampleInputPassword1"> 
                    </div>
                    <input type="submit" name="btnSubmit" class="btn btn-primary"></input>
                </form>
            </div>

            </div>
        </div>
    </div>
</div>
    </div>
</div>
<script>

</script>

<?php


?>