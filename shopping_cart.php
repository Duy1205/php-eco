﻿<?php
    session_start();
    include_once('header.php');
?>

<?php

    require_once("entities/product.class.php");
    require_once("entities/category.class.php");
    $cates = Category::list_category();

    error_reporting(E_ALL);
    ini_set('display_errors','1');

    if (isset($_GET["id"])){
        $pro_id = $_GET["id"];

        $was_found = false;
        $i = 0;

        if(!isset($_SESSION["cart_items"]) || count($_SESSION["cart_items"]) < 1){
            $_SESSION["cart_items"] = array( 0 => array("pro_id" => $pro_id, "quantity" => 1));
        }else{
            foreach( $_SESSION["cart_items"] as $item ){
                $i++;
                foreach( $item as $key => $value){
                    if( $key == "pro_id" && $value == $pro_id){
                        array_splice($_SESSION["cart_items"], $i-1, 1, array(array("pro_id" => $pro_id, "quantity" => $item["quantity"] + 1)));
                        $was_found = true;
                    }
                }
            }
            if( $was_found == false){
                array_push($_SESSION["cart_items"], array("pro_id" => $pro_id, "quantity" => 1));
            }
        }
        // header("location: shopping_cart.php");
    }
?>
    <nav class="breadcrumb-section theme1 bg-lighten2 pt-110 pb-110">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title text-center mb-15">
                        <h2 class="title text-dark text-capitalize">Giỏ hàng</h2>
                    </div>
                </div>
                <div class="col-12">
                    <ol class="breadcrumb bg-transparent m-0 p-0 align-items-center justify-content-center">
                        <li class="breadcrumb-item"><a href="/LAB03/index.php">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Giỏ hàng</li>
                    </ol>
                </div>
            </div>
        </div>
    </nav>
    <section class="whish-list-section theme1 pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="title mb-30 pb-25 text-capitalize">GIỎ HÀNG CỦA BẠN</h3>
                    
                    <nav id="sticky" class="header-bottom theme1 d-none d-lg-block">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="  position-relative">
                                <ul class="main-menu d-flex">
                                <?php 
                                    foreach ( $cates as $item){
                                        echo "<li > <a href=/LAB03/list_product.php?cateid=".$item["CateID"]."> ".$item["CategoryName"]." </a> </li>";
                                    }
                                ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>

                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                <th class="text-center" scope="col">Tên Sản Phẩm</th>
                                <th class="text-center" scope="col">Hình Ảnh</th>
                                <th class="text-center" scope="col">Số Lượng</th>
                                <th class="text-center" scope="col">Đơn Giá</th>
                                <th class="text-center" scope="col">Thành Tiền</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php
                                $total_money = 0;
                                if(isset( $_SESSION["cart_items"]) && count($_SESSION["cart_items"]) > 0 )
                                {
                                    foreach($_SESSION["cart_items"] as $item)
                                    {
                                        $id           = $item["pro_id"];
                                        $product      = Product::get_product($id);
                                        $prod         = reset($product);
                                        $total_money += $item["quantity"] * $prod["Price"];
                                        echo "
                                        <tr>
                                            <td class='text-center'>".$prod["ProductName"]."</td>
                                            <td class='text-center'>
                                                <img width='100px' height='100px' src=".$prod["Picture"].">
                                            </td>
                                            <td class='text-center'>".$item["quantity"]."</td>
                                            <td class='text-center'>".$prod["Price"]."</td>
                                            <td class='text-center'>".$prod["Price"]."</td>
                                            
                                            
                                        </tr>";
                                    }
                                    echo"
                                    <tr><td colspan=5 class='text-right'> 
                                        Tổng Tiền: $total_money
                                     </td></tr>
                                    ";
                                    echo "
                                    <tr><td colspan=5 class='text-right'> 
                                            <a href='/LAB03/list_product.php'><button class='btn btn-warning' type='button'>TIẾP TỤC MUA HÀNG</button></a>
                                        </td>
                                        </tr>
                                    ";
                                    echo"
                                    <tr>
                                    <td colspan=5 class='text-right'> 
                                        <button class='btn btn-warning' type='button'>THANH TOÁN</button>
                                    </td></tr>
                                    ";
                                }else{
                                    echo "Khong co san pham nao trong gio hang";
                                }
                                
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include_once('footer.php')
?>
