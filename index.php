<?php

    include_once('header.php')

?>
<!-- main slider start -->
<section class="bg-light position-relative">
    <div class="main-slider dots-style theme1">
        <div class="slider-item bg-img bg-img1">
            <div class="container">
                <div class="row align-items-center slider-height">
                    <div class="col-12">
                        <div class="slider-content">
                            <p class="text text-white text-uppercase animated mb-25" data-animation-in="fadeInDown">
                                nike running shoes</p>
                            <h4 class="title text-white animated text-capitalize mb-20" data-animation-in="fadeInLeft"
                                data-delay-in="1">Sport Shoes</h4>
                            <h2 class="sub-title text-white animated" data-animation-in="fadeInRight" data-delay-in="2">
                                Sale 40% Off</h2>
                            <a href="shop-grid-4-column.html"
                                class="btn theme--btn1 btn--lg text-uppercase rounded-5 animated mt-45 mt-sm-25"
                                data-animation-in="zoomIn" data-delay-in="3">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider-item end -->
        <div class="slider-item bg-img bg-img2">
            <div class="container">
                <div class="row align-items-center slider-height">
                    <div class="col-12">
                        <div class="slider-content">
                            <p class="text text-white text-uppercase animated mb-25" data-animation-in="fadeInLeft">
                                New Arrivals</p>
                            <h4 class="title text-white animated text-capitalize mb-20" data-animation-in="fadeInRight"
                                data-delay-in="1"> Sumer Sale</h4>
                            <h2 class="sub-title text-white animated" data-animation-in="fadeInUp" data-delay-in="2">Up
                                To 70% Off</h2>
                            <a href="shop-grid-4-column.html"
                                class="btn theme--btn1 btn--lg text-uppercase rounded-5 animated mt-45 mt-sm-25"
                                data-animation-in="zoomIn" data-delay-in="3">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider-item end -->
    </div>
    <!-- slick-progress -->
    <div class="slick-progress">
        <span></span>
    </div>
    <!-- slick-progress end-->
</section>
<section class="product-tab bg-white pt-50 pb-80">
    <div class="common-banner bg-white pb-50">
        <div class="container">
            <div class="col-12">
                        <div class="section-title text-center mb-30">
                            <h2 class="title text-dark text-capitalize">Sản Phẩm của Chúng Tôi</h2>
                            <p class="text mt-10">Add our products to weekly line up</p>
                        </div>
            </div>
            <div class="row">
                <?php
                    require_once("entities/product.class.php");
                    require_once("entities/category.class.php");

                    $prods = Product::list_product();
                    foreach($prods as $item){
                ?>
                <div class="col-lg-4 col-md-6 mb-30">
                    <div class="banner-thumb">
                        <div class="zoom-in d-block overflow-hidden position-relative">
                            <a href="/LAB03/product_detail.php?cateid=<?php echo $item["CateID"];?>&id=<?php echo $item["ProductID"];?>"> <img src="/LAB03/<?php echo $item["Picture"];?>" width="350px" height="350px" alt="banner-thumb-naile"></a>
                            <a href="/LAB03/shopping_cart.php?id=<?php echo $item["ProductID"]; ?>"
                                class="text-uppercase btn theme--btn1 btn--lg banner-btn position-absolute">Mua Hàng</a>
                        </div>
                        <h3><?php echo $item["ProductName"];?> - Giá : <?php echo $item["Price"];?> VNĐ</h3>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- main slider end -->
<!-- common banner  start -->

<?php

    include_once('footer.php')

?>